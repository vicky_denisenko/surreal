import { parallaxMouse, openMenu, inputLabel, validateForm, disableScroll, enableScroll, initCursor } from './functions';
import lottie from 'lottie-web';
import bodymovin from 'bodymovin';
import anime from 'animejs/lib/anime.es.js';
import Splitting from 'splitting';
import 'waypoints/lib/jquery.waypoints.min.js';
import paroller from 'paroller.js';
import inView from 'in-view';
import MicroModal from 'micromodal';

const chars = Splitting({ by: "chars", whitespace: true });

Splitting({ target: ".block--right .full-area .wrapper:last-child .desc blockquote .basic-text", by: "lines" });

document.addEventListener("DOMContentLoaded", function() {
  MicroModal.init({
    disableScroll: true,
    awaitCloseAnimation: true
  });

  if (window.innerWidth > 991) {
    parallaxMouse();
  }

  if (window.innerWidth > 767) {
    initCursor();
  }

  openMenu();
  inputLabel();
  validateForm();

  function removeHash() {
    history.pushState("", document.title, window.location.pathname + window.location.search);
  }

  let anchorLink = $(".js-anchor").click(function(e) {
    e.preventDefault();
    let $href = this.hash;
    let top = $($href).offset().top;

    enableScroll();

    $("body, html").animate({ scrollTop: top }, 800);
    $(".header__menu button").removeClass("clicked");
    $(".circles").removeClass("active");
    $(".header__nav ul").removeClass("active");

    anime.timeline()
      .add({
        targets: ".header__nav a .char",
        opacity: 0,
        duration: 1000,
        easing: "easeOutExpo"
      });
    anime.timeline()
      .add({
        targets: ".header__nav .abs-logo .overlay span",
        opacity: 0,
        easing: "easeInOutQuad",
        duration: 600,
        delay: (el, i) => 20 * (i + 1)
      });
  });
  setTimeout(removeHash, 1000);

  let illOne = lottie.loadAnimation({
    container: document.getElementById("ill-1"),
    renderer: "svg",
    loop: true,
    autoplay: true,
    path: "./data/why.json"
  });

  let illTwo = lottie.loadAnimation({
    container: document.getElementById("ill-2"),
    renderer: "svg",
    loop: true,
    autoplay: true,
    path: "./data/what.json"
  });

  let iconOne = lottie.loadAnimation({
    container: document.getElementById("iconOne"),
    renderer: "svg",
    loop: true,
    autoplay: true,
    path: "./data/icon-1.json"
  });

  let iconTwo = lottie.loadAnimation({
    container: document.getElementById("iconTwo"),
    renderer: "svg",
    loop: true,
    autoplay: true,
    path: "./data/icon-2.json"
  });

  anime.timeline()
    .add({
      targets: ".h1 span .word .char, .optional .word .char",
      opacity: 1,
      easing: "easeInOutQuad",
      duration: 600,
      delay: (el, i) => 50 * (i + 1)
    });

  setTimeout(function() {
    anime.timeline()
      .add({
        targets: ".block--right .full-area .wrapper:last-child .desc blockquote .basic-text .word",
        translateY: [100, 0],
        translateZ: 0,
        opacity: [0, 1],
        easing: "easeOutExpo",
        duration: 1000,
        delay: (el, i) => 100 + 30 * i,
        complete: function() {
          $(".block--right .full-area .wrapper:last-child .desc blockquote cite").addClass("visible");
        }
      });
  }, 1000);
});

if (window.innerWidth > 767) {
  inView(".hero-section").on("enter", () => {
    $(".hero-section").addClass("in-view");

    anime.timeline()
      .add({
        targets: ".header__nav .abs-logo .overlay span",
        opacity: 1,
        easing: "easeInOutQuad",
        duration: 200,
        delay: (el, i) => 20 * (i + 1)
      });

    setTimeout(function() {
      $(".header__nav .abs-logo").addClass("hidden");
    }, 200);
  });

  inView.offset(100);
  inView(".hero-section").on("exit", () => {
    $(".hero-section").removeClass("in-view");

    $(".header__nav .abs-logo").removeClass("hidden");

    setTimeout(function() {
      anime.timeline()
        .add({
          targets: ".header__nav .abs-logo .overlay span",
          opacity: 0,
          easing: "easeInOutQuad",
          duration: 1400,
          delay: (el, i) => 300 + 30 * i
        });
    }, 200);
  });
}

$(window).scroll(function() {
  if ($(".header__nav ul").hasClass("active")) {
    $(".header__nav .abs-logo").addClass("hidden");
  } else {
    $(".header__nav .abs-logo").removeClass("hidden");
  }
});

new Waypoint({
  element: document.querySelector(".second-screen"),
  handler: function(direction) {
    if (direction == "down") {
      anime.timeline()
        .add({
          targets: ".second-screen h2 .char",
          opacity: 1,
          easing: "easeInOutQuad",
          duration: 600,
          delay: (el, i) => 150 * (i + 1)
        });

      anime.timeline()
        .add({
          targets: ".second-screen .item-text .basic-text .word",
          translateY: [100, 0],
          translateZ: 0,
          opacity: [0, 1],
          easing: "easeOutExpo",
          duration: 1400,
          delay: (el, i) => 300 + 30 * i
        });
    }
  },
  offset: "60%"
});

new Waypoint({
  element: document.querySelector(".third-screen"),
  handler: function(direction) {
    if (direction == "down") {
      $(".js-animation-wrap").addClass("fadeup");
      setTimeout(function() {
        $(".js-animation-wrap").addClass("animated");
      }, 3000);
    }
  },
  offset: "90%"
});

$(".third-screen .text-block .item").each(function() {
  let self = $(this);

  $(this).waypoint({
    handler: function(direction) {
      if (direction == "down") {
        self.addClass("fadeup");
      }
    },
    offset: "50%"
  });
});

new Waypoint({
  element: document.querySelector(".about-section h2"),
  handler: function(direction) {
    if (direction == "down") {
      anime.timeline()
        .add({
          targets: ".about-section h2 .char",
          opacity: 1,
          easing: "easeInOutQuad",
          duration: 600,
          delay: (el, i) => 150 * (i + 1)
        });
      anime.timeline()
        .add({
          targets: ".about-section .basic-text .word",
          translateY: [100, 0],
          translateZ: 0,
          opacity: [0, 1],
          easing: "easeOutExpo",
          duration: 1400,
          delay: (el, i) => 300 + 30 * i
        });

      $(".about-section .inner .item:last-child").addClass("visible");
    }
  },
  offset: "70%"
});

window.addEventListener("load", () => {
  setTimeout(() => document.querySelector('html').scrollTo(0, 0), 1);
});
