import { settings } from './settings';
import anime from 'animejs/lib/anime.es.js';

function preventDefault(e) {
  e.preventDefault();
}

export function disableScroll() {
  document.body.addEventListener("touchmove", preventDefault, {
    passive: false
  });
}

export function enableScroll() {
  document.body.removeEventListener("touchmove", preventDefault);
}

export function parallaxMouse() {
  const parallaxMouse = (parent, element) => {

    let _elements = parent.querySelectorAll(element);
    let _offset = parent.getBoundingClientRect();
    let _dataAxe = parent.dataset.paralaxAxe;

    parent.addEventListener('mousemove', (ev) => {
      const _mousePos = {
        top: ev.pageY - _offset.top,
        left: ev.pageX - _offset.left
      };

      Array.prototype.forEach.call(_elements, (el) => {
        const _elData = el.dataset.parallaxOffset;

        const _strength = parseInt(_elData);

        let _traX = (_mousePos.left - (_offset.width / 3)) * _strength / _offset.width;
        let _traY = (_mousePos.top - (_offset.height / 3)) * _strength / _offset.height;

        if (_dataAxe === 'x') {
          _traY = '0';
        } else if (_dataAxe === 'y') {
          _traX = '0';
        }

        el.style.transform = "translate(" + _traX + "px, " + _traY + "px)";
      });
    });
  };

  let _parallax = document.querySelector(".js-parallax");
  let _elements = ".js-parallaxItem";
  parallaxMouse(_parallax, _elements);
}

export function openMenu() {

  let button = document.querySelectorAll(".header__menu button")[0];

  button.addEventListener("click", function() {
    if (!$(this).hasClass("clicked")) {
      $(this).addClass("clicked");
      $(".circles").addClass("active");
      $(".header__nav ul").addClass("active");
      setTimeout(function() {
        $(".header__nav .abs-logo").addClass("hidden");
      }, 1000)

      anime.timeline()
        .add({
          targets: ".header__nav a .char",
          opacity: 1,
          easing: "easeInOutQuad",
          duration: 600,
          delay: (el, i) => 20 * (i + 1)
        });

      if (!$(".hero-section").hasClass("in-view")) {
        anime.timeline()
          .add({
            targets: ".header__nav .abs-logo .overlay span",
            opacity: 1,
            easing: "easeInOutQuad",
            duration: 200,
            delay: (el, i) => 20 * (i + 1)
          });
      }
    } else {
      $(this).removeClass("clicked");
      $(".circles").removeClass("active");
      $(".header__nav ul").removeClass("active");
      setTimeout(function() {
        $(".header__nav .abs-logo").removeClass("hidden");
      }, 1000)

      anime.timeline()
        .add({
          targets: ".header__nav a .char",
          opacity: 0,
          duration: 1000,
          easing: "easeOutExpo"
        });

      if (!$(".hero-section").hasClass("in-view")) {
        anime.timeline()
          .add({
            targets: ".header__nav .abs-logo .overlay span",
            opacity: 0,
            easing: "easeInOutQuad",
            duration: 600,
            delay: (el, i) => 20 * (i + 1)
          });
      }
    }

    if (window.innerWidth < 767) {
      if (!$(".header__nav ul").hasClass("active")) {
        enableScroll();
      } else {
        disableScroll();
      }
    }

  });

  $(document).mouseup(function(e) {
    let header = $(".header__layout");
    let btn = $(".header__menu button");
    let circle = $(".circles");
    let nav = $(".header__nav ul");

    if (!header.is(e.target) && header.has(e.target).length === 0) {
      btn.removeClass("clicked");
      circle.removeClass("active");
      nav.removeClass("active");
      setTimeout(function() {
        $(".header__nav .abs-logo").removeClass("hidden");
      }, 1000)

      anime.timeline()
        .add({
          targets: ".header__nav a .char",
          opacity: 0,
          duration: 1000,
          easing: "easeOutExpo"
        });

      if (!$(".hero-section").hasClass("in-view")) {
        anime.timeline()
          .add({
            targets: ".header__nav .abs-logo .overlay span",
            opacity: 0,
            easing: "easeInOutQuad",
            duration: 600,
            delay: (el, i) => 20 * (i + 1)
          });
      }
    }
  });
}

export function inputLabel() {
  function classReg(className) {
    return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
  }

  let hasClass, addClass, removeClass;

  if ('classList' in document.documentElement) {
    hasClass = function(elem, c) {
      return elem.classList.contains(c);
    };
    addClass = function(elem, c) {
      elem.classList.add(c);
    };
    removeClass = function(elem, c) {
      elem.classList.remove(c);
    };
  } else {
    hasClass = function(elem, c) {
      return classReg(c).test(elem.className);
    };
    addClass = function(elem, c) {
      if (!hasClass(elem, c)) {
        elem.className = elem.className + ' ' + c;
      }
    };
    removeClass = function(elem, c) {
      elem.className = elem.className.replace(classReg(c), ' ');
    };
  }

  function toggleClass(elem, c) {
    let fn = hasClass(elem, c) ? removeClass : addClass;
    fn(elem, c);
  }

  let classie = {
    // full names
    hasClass: hasClass,
    addClass: addClass,
    removeClass: removeClass,
    toggleClass: toggleClass,
    // short names
    has: hasClass,
    add: addClass,
    remove: removeClass,
    toggle: toggleClass
  };

  [].slice.call(document.querySelectorAll('.input__field')).forEach(function(inputEl) {
    // in case the input is already filled.
    if (inputEl.value.trim() !== '') {
      classie.add(inputEl.parentNode, 'input--filled');
    }

    // events:
    inputEl.addEventListener('focus', onInputFocus);
    inputEl.addEventListener('blur', onInputBlur);
  });

  function onInputFocus(ev) {
    classie.add(ev.target.parentNode, 'input--filled');
  }

  function onInputBlur(ev) {
    if (ev.target.value.trim() === '') {
      classie.remove(ev.target.parentNode, 'input--filled');
    }
  }
}

function ajax(method, url, data, success, error) {
  var xhr = new XMLHttpRequest();
  xhr.open(method, url);
  xhr.setRequestHeader("Accept", "application/json");
  xhr.onreadystatechange = function() {
    if (xhr.readyState !== XMLHttpRequest.DONE) return;
    if (xhr.status === 200) {
      success(xhr.response, xhr.responseType);
    } else {
      error(xhr.status, xhr.response, xhr.responseType);
    }
  };
  xhr.send(data);
}

export function validateForm() {
  let form = document.querySelector(".contact-form");
  let regular = {
    email: /^([a-zA-Z0-9_\-\.\+]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,64})$/,
    phone: /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{6,10}$/
  };
  let status = document.getElementById("my-form-status");

  let validationFieldName = ['phone', 'email', 'name', 'textarea'];

  let regularError = {
    email: 'Email is invalid',
    name: 'Required field',
    phone: 'Phone number is invalid',
    textarea: 'Required field'
  };

  const toggleCondition = (isValid, el, text) => {
    isValid ? (el.parentNode.classList.remove('error'), el.nextElementSibling.innerHTML = '') : (el.parentNode.classList.add('error'), el.nextElementSibling.innerHTML = text);
    return isValid;
  };

  //functions
  function success() {
    form.reset();
    $(".input").removeClass("input--filled");
    status.innerHTML = "Thanks! We will read your letter and will answer.";
    // setTimeout(function() {
    //   status.classList.add("hide");
    // }, 1500);
  }

  function error() {
    status.innerHTML = "Oops! There was a problem.";
  }

  // submit
  $(".contact-form").on("submit", function(e) {
    e.preventDefault();

    const getInput = name => form.querySelector(`[name='${name}']`);

    const errorList = validationFieldName.reduce((acc, val) => Object.assign(acc, {
      [val]: true
    }), {});

    validationFieldName.forEach(name => {
      const el = getInput(name);
      const {
        value
      } = el;
      const text = name === 'phone' ? 'Enter phone number' : 'Required field';

      errorList[name] = toggleCondition(!!value.length, el, text);
    });

    const formIsEmpty = Object.values(errorList).every(value => !value);

    if (!formIsEmpty) {
      validationFieldName.forEach(name => {
        const el = getInput(name);
        const {
          value
        } = el;
        if (regular[name])
          errorList[name] = toggleCondition(regular[name].test(value), el, regularError[name])
      })
    }

    // finally
    const isSucess = Object.values(errorList).every(value => value);

    if (isSucess) {
      let data = new FormData(form);
      ajax(form.method, form.action, data, success, error);
    }

    return false;
  });
}

export function initCursor() {
  const cursor = {
    delay: 8,
    _x: 0,
    _y: 0,
    endX: (window.innerWidth / 2),
    endY: (window.innerHeight / 2),
    cursorVisible: true,
    cursorEnlarged: false,
    $dot: document.querySelector('.cursor-dot'),
    $outline: document.querySelector('.cursor-dot-outline'),

    init: function() {
      // Set up element sizes
      this.dotSize = this.$dot.offsetWidth;
      this.outlineSize = this.$outline.offsetWidth;

      this.setupEventListeners();
      this.animateDotOutline();
    },

    setupEventListeners: function() {
      let self = this;

      // Anchor hovering
      document.querySelectorAll("a, button").forEach(function(el) {
        el.addEventListener('mouseover', function() {
          self.cursorEnlarged = true;
          self.toggleCursorSize();
        });
        el.addEventListener('mouseout', function() {
          self.cursorEnlarged = false;
          self.toggleCursorSize();
        });
      });

      // Click events
      document.addEventListener('mousedown', function() {
        self.cursorEnlarged = true;
        self.toggleCursorSize();
      });
      document.addEventListener('mouseup', function() {
        self.cursorEnlarged = false;
        self.toggleCursorSize();
      });


      document.addEventListener('mousemove', function(e) {
        // Show the cursor
        self.cursorVisible = true;
        self.toggleCursorVisibility();

        // Position the dot
        self.endX = e.pageX;
        self.endY = e.pageY;
        self.$dot.style.top = self.endY + 'px';
        self.$dot.style.left = self.endX + 'px';
      });

      // Hide/show cursor
      document.addEventListener('mouseenter', function(e) {
        self.cursorVisible = true;
        self.toggleCursorVisibility();
        self.$dot.style.opacity = 1;
        self.$outline.style.opacity = 1;
      });

      document.addEventListener('mouseleave', function(e) {
        self.cursorVisible = true;
        self.toggleCursorVisibility();
        self.$dot.style.opacity = 0;
        self.$outline.style.opacity = 0;
      });
    },

    animateDotOutline: function() {
      let self = this;

      self._x += (self.endX - self._x) / self.delay;
      self._y += (self.endY - self._y) / self.delay;
      self.$outline.style.top = self._y + 'px';
      self.$outline.style.left = self._x + 'px';

      requestAnimationFrame(this.animateDotOutline.bind(self));
    },

    toggleCursorSize: function() {
      let self = this;

      if (self.cursorEnlarged) {
        self.$dot.style.transform = 'translate(-50%, -50%) scale(0.75)';
        self.$outline.style.transform = 'translate(-50%, -50%) scale(1.5)';
      } else {
        self.$dot.style.transform = 'translate(-50%, -50%) scale(1)';
        self.$outline.style.transform = 'translate(-50%, -50%) scale(1)';
      }
    },

    toggleCursorVisibility: function() {
      let self = this;

      if (self.cursorVisible) {
        self.$dot.style.opacity = 1;
        self.$outline.style.opacity = 1;
      } else {
        self.$dot.style.opacity = 0;
        self.$outline.style.opacity = 0;
      }
    }
  };

  cursor.init();
}
